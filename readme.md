## 适用范围
- 需要使用`phpword` 但又没有安装`compower`或者不喜欢`composer`的童鞋
- phpword 源码地址 [github](https://github.com/PHPOffice/PHPWord)
- phexcel 源码地址 [github](https://github.com/PHPOffice/PHPExcel)
- common 源码地址 [gighub](https://github.com/PHPOffice/Common)

> 为什么不是 PhpSpreadsheet 为了向下兼容^_^

## 方法
- 获取common公共文件 `src/Common`
- 获取phpword类文件`src/PhpWord`
- 获取phpexcel类文件`Classes`
- 组装成如下目录
```
phpoffice(任意目录)
│	├─common （公共类）
│	├─phpexcel 
│	├─phpword 
│	├─phpexcel.php 
│	└─phpword.php
└─readme.md
```

## 构造加载文件
```
<?php
spl_autoload_register(function ($class) {
    $class = ltrim($class, '\\');
    $prefix = 'PhpOffice\PhpWord';
    if (strpos($class, $prefix) === 0) {
        $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        $class = implode(DIRECTORY_SEPARATOR, array('phpword','PhpWord')) . substr($class, strlen($prefix));
        $file = __DIR__ . DIRECTORY_SEPARATOR . $class . '.php';
        var_dump($file);
        if (file_exists($file)) {
            require_once $file;
        }
    }
});

require_once __DIR__ . "/common/Autoloader.php";
\PhpOffice\Common\Autoloader::register();
```

## 使用
```
include('phpoffice/phpword.php');
$PHPWord = new \PhpOffice\PhpWord\PhpWord();
# code...干你该干的事
```

## 技巧
- phpword替换图片
```
include('phpoffice/phpword.php');

$PHPWord = new \PhpOffice\PhpWord\PhpWord();
//加载
$tempPlete = $PHPWord->loadTemplate($doc_file);
//替换 word里的字符为 ${abc} 
//照片
$tempPlete->setImageValue('photo_url', ['path' => $v,'width'=>120,'height'=>170]); 
//普通
$tempPlete->setValue('field','abc');
//输出
$filename = "[{$data['cat_text']}]-{$data['name']}.docx";
header('Content-Type: application/vnd.ms-word');
header('Content-Disposition: attachment;filename='.$filename);
header('Cache-Control: max-age=0');
$tempPlete->saveAs('php://output');//直接下载
//保存
//$tempPlete->saveAs($save_path.(iconv('utf-8','GB2312//IGNORE',$filename)));
```

- phpexcel使用

```
require_once('phpoffice/phpexcel.php');
$objPHPExcel = new \PHPExcel();
# code...干你该干的事
$filename = "xxx.xls";
$objWrite = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWrite->save($save_path.(iconv('utf-8','GB2312//IGNORE',$filename)));
return false;
```
