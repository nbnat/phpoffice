<?php
spl_autoload_register(function ($class) {
    $class = ltrim($class, '\\');
    $prefix = 'PhpOffice\PhpWord';
    if (strpos($class, $prefix) === 0) {
        $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        $class = implode(DIRECTORY_SEPARATOR, array('phpword','PhpWord')) . substr($class, strlen($prefix));
        $file = __DIR__ . DIRECTORY_SEPARATOR . $class . '.php';
        var_dump($file);
        if (file_exists($file)) {
            require_once $file;
        }
    }
});

require_once __DIR__ . "/common/Autoloader.php";
\PhpOffice\Common\Autoloader::register();